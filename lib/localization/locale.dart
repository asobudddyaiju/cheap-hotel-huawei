import 'package:flutter/cupertino.dart';
import 'package:cheap_hotels_huawei/localization/demo_local.dart';

String getTranslated(BuildContext context, String key) {
  return DemoLocalizations.of(context).getTranslatedValues(key);
}
