import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hive/hive.dart';
import 'package:cheap_hotels_huawei/localization/locale.dart';
import 'package:cheap_hotels_huawei/src/ui/screens/about_us.dart';
import 'package:cheap_hotels_huawei/src/ui/screens/currency.dart';
import 'package:cheap_hotels_huawei/src/ui/screens/home_screen.dart';
import 'package:cheap_hotels_huawei/src/ui/screens/region.dart';
import 'package:cheap_hotels_huawei/src/ui/widgets/build_dropcurrency.dart';
import 'package:cheap_hotels_huawei/src/ui/widgets/build_dropdown.dart';
import 'package:cheap_hotels_huawei/src/ui/widgets/profile_list_tile.dart';
import 'package:cheap_hotels_huawei/src/utils/constants.dart';
import 'package:cheap_hotels_huawei/src/utils/object_factory.dart';
import 'package:cheap_hotels_huawei/src/ui/screens/login.dart';
import 'package:cheap_hotels_huawei/src/utils/utils.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  String buttonName = "Sign_in";
  String defaultLang = Platform.localeName;

  String photpurl;
  String dummyphotourl =
      'https://upload.wikimedia.org/wikipedia/commons/thumb/d/d8/Antu_system-switch-user.svg/768px-Antu_system-switch-user.svg.png';
  String displayName;
  String dummydisplayName = 'Username';
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Constants.kitGradients[3],
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenHeight(context, dividedBy: 20)),
          // here the desired height
          child: AppBar(
            leading: new IconButton(
              icon: new Icon(
                Icons.close,
                color: Colors.black,
              ),
              onPressed: () {
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(builder: (context) => HomePage()),
                        (route) => false);
              },
            ),
            // title: Text(
            //   getTranslated(context, 'Profile'),
            //   style: TextStyle(
            //       color: Colors.black54, fontWeight: FontWeight.normal),
            // ),
            elevation: 0,
            backgroundColor: Colors.white,
          )),
      body: ListView(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 10, left: 30,right: 30),
            child: Row(
              children: [
                Text(
                  getTranslated(context, 'Welcome_to')+' ',
                  style: TextStyle(
                      fontSize: 24,
                      color: Color(0xFF272727),
                      fontFamily: 'Gilroy'),
                ),
                Text(
                  getTranslated(context, 'Hotels')+"!",
                  style: TextStyle(
                      fontSize: 24,
                      color: Constants.kitGradients[0],
                      fontFamily: 'Nexa',
                      fontWeight: FontWeight.w900
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
              left: 8,
              right: 8,
            ),
            child: Container(
                width: screenWidth(context, dividedBy: 1.2),
                height: screenHeight(context, dividedBy: 2.3),
                child: SvgPicture.asset("assets/images/Profile.svg",fit: BoxFit.fill,)),
          ),

          Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.only(
                        left: screenWidth(context, dividedBy: 20),
                        right: screenWidth(context, dividedBy: 20),
                        top: screenHeight(context, dividedBy: 75)),
                    child: Text(
                      getTranslated(context, 'language'),
                      style: TextStyle(fontSize: 16, color: Colors.black),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: screenHeight(context, dividedBy: 75),
                        right: screenWidth(context, dividedBy: 30),
                        left: screenWidth(context, dividedBy: 30)),
                    child: GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => RegionPage()));
                      },
                      child: Text(
                        Hive.box('lang').get(2) != null
                            ? Hive.box('lang').get(2)
                            : ObjectFactory().getLanguage.getCountryLanguage(defaultLang)[1],
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                  )
                ],
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 50),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.only(
                        left: screenWidth(context, dividedBy: 20),
                        right:screenWidth(context, dividedBy: 20),
                        top: screenHeight(context, dividedBy: 100)),
                    child: Text(
                      getTranslated(context, 'Currency'),
                      style: TextStyle(fontSize: 16, color: Colors.black),
                    ),
                  ),
                  Padding(
                      padding: EdgeInsets.only(
                          top: screenHeight(context, dividedBy: 75),
                          right: screenWidth(context, dividedBy: 30),
                          left: screenWidth(context, dividedBy: 30),
                          bottom: screenHeight(context, dividedBy: 75)),
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => CurrencyPage()));
                        },
                        child: Text(
                          Hive.box('code').get(2) != null
                              ? Hive.box('code').get(2).toString().split(" ").first
                              : Hive.box('room').get('country')!=null?ObjectFactory().getCurrency.getCurrency(Hive.box('room').get('currency')):"USD",
                          style: TextStyle(fontSize: 16),
                        ),
                      )),
                ],
              ),
              Padding(
                padding:  EdgeInsets.symmetric(horizontal: screenWidth(context,dividedBy: 20)),
                child: Container(
                  height: screenWidth(context,dividedBy: 250),
                  color: Constants.kitGradients[4].withOpacity(.30),
                ),
              )
            ],
          ),
          Column(children: [
            GestureDetector(
              onTap: (){
    Navigator.push(context,
    MaterialPageRoute(builder: (context) => AboutPage()));
    }
              ,
              child: Container(
                height: screenHeight(context, dividedBy: 15),
                child: ProfileListTile(
                  listText: getTranslated(context, 'About_Us'),
                  listText1: '',
                ),
              ),
            ),
            Padding(
              padding:  EdgeInsets.symmetric(horizontal: screenWidth(context,dividedBy: 20)),
              child: Container(
                height: screenWidth(context,dividedBy: 250),
                color: Constants.kitGradients[4].withOpacity(.30),
              ),
            ),

            Container(
              height: screenHeight(context, dividedBy: 15),
              child: ProfileListTile(
                listText: getTranslated(context, 'Help_and_feedback'),
                listText1: '',
              ),
            ),
            Padding(
              padding:  EdgeInsets.symmetric(horizontal: screenWidth(context,dividedBy: 20)),
              child: Container(
                height: screenWidth(context,dividedBy: 250),
                color: Constants.kitGradients[4].withOpacity(.30),
              ),
            ),

            Container(
              height: screenHeight(context, dividedBy: 15),
              child: ProfileListTile(
                listText: getTranslated(context, 'Terms_and_conditions'),
                listText1: '',
              ),
            ),
            Padding(
              padding:  EdgeInsets.symmetric(horizontal: screenWidth(context,dividedBy: 20)),
              child: Container(
                height: screenWidth(context,dividedBy: 250),
                color: Constants.kitGradients[4].withOpacity(.30),
              ),
            ),

            Container(
              height: screenHeight(context, dividedBy: 15),
              child: ProfileListTile(
                listText: getTranslated(context, 'Privacy_policy'),
                listText1: '',
              ),
            ),
            Padding(
              padding:  EdgeInsets.symmetric(horizontal: screenWidth(context,dividedBy: 20)),
              child: Container(
                height: screenWidth(context,dividedBy: 250),
                color: Constants.kitGradients[4].withOpacity(.30),
              ),
            ),
            SizedBox(height: screenWidth(context,dividedBy: 60))
          ]),
        ],
      ),
    );
  }
}
