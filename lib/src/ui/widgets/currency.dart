import 'package:find_dropdown/find_dropdown.dart';
import 'package:flutter/material.dart';

class Currency extends StatefulWidget {
  static const routeName = '/drop-down';

  @override
  _CurrencyState createState() => _CurrencyState();
}

class _CurrencyState extends State<Currency> {
  List<String> currencies = [
    "AED",
    "BGN",
    "BRL",
    "CNY",
    "CZK",
    "HRK",
    "DKK",
    "EUR",
    "HUF",
    "HKD",
    "ISK",
    "IDR",
    "ILS",
    "JPY",
    "KRW",
    "LTL",
    "MYR",
    "NOK",
    "PHP",
    "PLN",
    "RON",
    "RUB",
    "RSD",
    "SEK",
    "THB",
    "TRY",
    "UAH",
    "USD",
    "VND",
  ];
  String _currency = "";

  @override
  Widget build(BuildContext context) {
    return FindDropdown(
        onChanged: (value) {
          _currency = value;
        },
        items: currencies,
        selectedItem: _currency);
  }
}
