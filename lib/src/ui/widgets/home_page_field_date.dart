import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:cheap_hotels_huawei/src/ui/widgets/date_range_picker.dart';
import 'package:cheap_hotels_huawei/src/utils/utils.dart';

// ignore: must_be_immutable
class HomePageDate extends StatefulWidget {
  String label;
  String hintText;
  Widget icon;
  Function onTap;
  HomePageDate({
    this.label,
    this.onTap,
    this.hintText,
    this.icon
  });
  @override
  _HomePageDateState createState() => _HomePageDateState();
}

class _HomePageDateState extends State<HomePageDate> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:  EdgeInsets.symmetric(horizontal: screenWidth(context,dividedBy: 15)),
      child: Container(
        height: screenHeight(context,dividedBy: 9),
        width: screenWidth(context,dividedBy: 1),
        child: Row(
          children: [
            Padding(
              padding: EdgeInsets.only(
                  top: screenHeight(context, dividedBy: 50)),
              child: Container(
                height: 60,
                width: 60,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30),
                  color: Color(0xFFE3E4E8),
                ),
                child: Padding(
                    padding: const EdgeInsets.all(10),
                    child:widget.icon
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(
                  top: screenHeight(context, dividedBy: 30), left: 10),
              child: Container(
                height: screenHeight(context, dividedBy: 11),
                width: screenWidth(context, dividedBy: 1.5),
                // color: Colors.transparent,
                child: Column(
                  children: [
                    Padding(
                      padding:  EdgeInsets.only(top: screenWidth(context,dividedBy: 100)),
                      child: Container(
                        height: screenHeight(context, dividedBy: 38),
                        width: screenWidth(context, dividedBy: 1),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(25),
                          color: Colors.transparent,
                        ),
                        child: Text(
                          widget.label,
                          style: TextStyle(
                              color: Color(0xFFABABAB),
                              fontSize: 14,
                              fontFamily: 'poppins'),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(
                          // vertical: screenHeight(context, dividedBy: 120),
                          // horizontal: screenWidth(context,dividedBy: 100)
                    ),
                      child: Container(
                        height: screenHeight(context,dividedBy: 25),
                        width: screenWidth(context,dividedBy: 1),
                        child: DateRangeField(
                          fieldVal: widget.hintText,
                          context: context,
                          // initialValue: DateTimeRange(
                          //   start: DateTime.now().add(Duration(days:0)),
                          //   end: DateTime.now().add(Duration(days:1)),
                          // ),
                        )
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
